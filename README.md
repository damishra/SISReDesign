# SIS Redesign
This is our vision for what the user interaction with the SIS should feel like. We have not implemented all the features as this is only meant to demonstrate the login and enrollment in the SIS.

# Members
+ Dishant Mishra
+ Tenzin Dhondup
+ Andrew Berson
+ Andrew Costanzo
+ Clare Truell
